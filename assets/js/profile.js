
let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let editButton = document.querySelector("#editButton")
// get the token from the local storage
let token = localStorage.getItem('token')
let courseArray = [];

let profileName = document.querySelector("#profileName")
let profileDesc = document.querySelector("#profileDesc")
let profileEmail = document.querySelector("#profileEmail")
let profilePhoneNum = document.querySelector("#profilePhoneNum")
let profileEnrollments = document.querySelector("#profileEnrollments")
let enrollments = document.querySelector("#enrollments")
// let enrollContainer = document.querySelector("#enrollContainer")


// get details of currently logged in user
fetch(`https://immense-wildwood-11110.herokuapp.com/api/users/details`,{
	headers: {
		Authorization: `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	/*
	console.log(data)
	console.log(data.firstName, data.lastName)
	console.log(data.isAdmin)
	console.log(data.enrollments)
	console.log(data.enrollments[0])
	console.log(data.enrollments[0].courseId)
	console.log(data.enrollments.length)
	console.log(data.enrollments.map(e => e)
	*/

	profileName.innerHTML = `${data.firstName} ${data.lastName}`

	if (data.isAdmin === true) {
		profileDesc.innerHTML = `Administrator`
	} else {
		profileDesc.inerHTML = `Regular User`
	}

	profileEmail.innerHTML = data.email
	profilePhoneNum.innerHTML = data.mobileNo
	profileEnrollments.innerHTML = data.enrollments

	if (data.enrollments.length < 1) {
		if (data.isAdmin === true) {
			enrollments.innerHTML = `
			<p>You have the ability to administer courses.</p>
			<a href="./courses.html" class="btn btn-primary">Courses Page</a>
			`;
		} else {
		profileEnrollments.innerHTML = `
		<p>Not yet enrolled in any courses.</p>
		<a href="./courses.html" class="btn btn-outline-primary mt-3">Book Now!</a>
		`
		}
	} else {
		let courseId
		let courseNames = ""
		for (i = 0; i < data.enrollments.length; i++) {
			courseId = data.enrollments[i].courseId
			console.log(courseId)

			fetch(`https://immense-wildwood-11110.herokuapp.com/api/courses/${courseId}`)
			.then(res => res.json())
			.then(data => {
				console.log(data.name)
				courseNames += `${data.name} <br>`
				profileEnrollments.innerHTML = `${courseNames}`
			})
		}
	}
})




