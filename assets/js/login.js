let loginForm = document.querySelector("#logInUser");

loginForm.addEventListener("submit", e => {
	e.preventDefault();
	
	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value

	console.log(email)
	console.log(password)

	if (email == "" || password == "") {
		alert("Email/Password field empty.")
	} else {
		fetch("https://immense-wildwood-11110.herokuapp.com/api/users/login", {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data) //<- shows the token in the console

			// add if-else statement that will verify our users they have logged in successfully and got their tokens
			// if the user cannot log in successully, they will get an alert.
			// if the user has logged in successfully, we will save their token into the web browser and get their details using fetch
			// to save data into the web browser, we access the localStorage. 
			// The localStorage is used to save data into the web browser
			// this is storage is read only, therefore, we set our data into the localStorage throught javascript by providing a key/value pair into the storage
			// the method setItem() for localStorage allows to save data into the localStorage. 
			// syntax: 
			// localStorage.setItem("key",data)

			// to get an item from the localStorage, here's the syntax:
			// localStorage.getItem("key")

			if(data.accessToken){
				// alert("Logged in successfully.")
				// set the token into the local storage
				localStorage.setItem('token',data.accessToken)
			
				// send a fetch request to decode the JWT and obtain the user ID and isAdmin property.
				fetch('https://immense-wildwood-11110.herokuapp.com/api/users/details',{
					//method: 'GET', <-kapag get yung method, di na need ilagay yung method and yung body. di na din need i-define yung content type sa headers
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json()) //<-arrow function, kapag walang curly brace, automatic mag rereturn
				.then(data => {
					console.log(data)

					localStorage.setItem('isAdmin',data.isAdmin,)
					localStorage.setItem('id',data._id)

					//window.location.replace = this allows to redirect our user to another page
					window.location.replace("./courses.html")					
				})
			} else {
				alert("Login failed. Something went wrong.")
			}
		})
	}
}) 
