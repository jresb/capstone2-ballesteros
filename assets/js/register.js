// Document Object Model DOM
let registerForm = document.querySelector("#registerUser");

registerForm.addEventListener("submit", e => {
	e.preventDefault()
	console.log("Submit event triggered.")

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNo = document.querySelector("#mobileNumber").value;
	let email = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11) && firstName !== '' && lastName !== '') {
		
		// fetch (url, options)
		fetch('https://immense-wildwood-11110.herokuapp.com/api/users/email-exists',{
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log("email already exists:", data) //returns true if the email already exists
			if (data === false) {
				fetch('https://immense-wildwood-11110.herokuapp.com/api/users', {
					method: 'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => res.json()) /*string to json*/
				.then(data => {
					console.log("forms obtained successfully:", data) //returns true if data in the forms are obtained succcessfully
				
					if(data === true) {
						alert("Registration successful.")

						// redirect to login page
						window.location.replace("./login.html")
					} else {
						// error in creating Registration
						alert("Registration failed.")
					}
				})
			} else {
				alert("Email already exists.")
			}
		})
	} else {
		alert("Passwords don't match / Mobile No. should be 11 digits.")
	}

})