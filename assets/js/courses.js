let token = localStorage.getItem("token")
let adminUser = localStorage.getItem("isAdmin") === "true"
let addButton = document.querySelector("#adminButton")
let cardFooter
let arrayCourse = [];
let userEnrollments = [];

function checkCourse () {
	fetch('https://immense-wildwood-11110.herokuapp.com/api/courses/')
	.then(res => res.json()) //string to json
	.then(data => {
		console.log("array of courses:")
		console.log(data)

		// a variable that wil store the data to be rendered
		let courseData;

		if (data.length < 1) {
			courseData = "No courses available at this time."
		} else {
			courseData = data.map(course => {
				return course
			})
		}
	})
}

//checks if course._id is present in the enrollments of the user currently logged in
function checkIfEnrolled(a,b) {	
	fetch(`https://immense-wildwood-11110.herokuapp.com/api/users/details`, {
		headers: {
			Authorization: `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		// console.log(data)
		let isEnrolled = data.enrollments.map(x => x.courseId).includes(a)
		console.log(`Is ${data.firstName} currently enrolled in ${b}?: ${isEnrolled}`)
		return isEnrolled
	})
	.then(data => {
		// console.log(data)
	})
	.catch(err => {
		console.log("Not logged in.")
	})
}


//This if statement will allow us to show a button for adding a course;a button to redirect us to the addCourse page if the user is admin, however, if he/she is a guest or a regular user, they should not see a button.
if(adminUser === false || adminUser === null){
	addButton.innerHTML = null
} else {
	addButton.innerHTML = `
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
		</div>
	`
}

fetch('https://immense-wildwood-11110.herokuapp.com/api/courses/')
.then(res => res.json()) //string to json
.then(data => {
	console.log("array of courses:")
	console.log(data)

	// a variable that wil store the data to be rendered
	let courseData;

	if (data.length < 1) {
		courseData = "No courses available at this time."
	} else {
		courseData = data.map(course => {
			console.log(course);
			let a = course._id
			let b = course.name
			//checkIfEnrolled(a,b) <-shows if current user is enrolled in the course
			arrayCourse.push(course._id) //<-stores the courses in an array
			console.log(arrayCourse)
			// localStorage.setItem('arrayCourse', arrayCourse)
			
			//regular user
			if(adminUser == false || !adminUser) {
				
				if (course.isActive === true) {

					checkIfEnrolled(a,b)
					cardFooter =
						`
							<a href="./course.html?courseId=${a}" value={course._id} class="btn btn-primary text-white btn-block editButton">Select Course</a>
						`

					return(
						`
							<div class="col-md-6 my-3">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title">
											${course.name}
										</h5>
										<p class="card-text text-left">
											${course.description}
										</p>
										<p class="card-text text-right">
											₱${course.price}
										</p>
									</div>
									<div class="card-footer">
										${cardFooter}
									</div>
								</div>
							</div>
						`
					)	
				}
			
			//admin
			} else {
				if (course.isActive === true) {				
					cardFooter =
						`
							<a href="./viewCourse.html?courseId=${course._id}" value={course._id} class="btn btn-success text-white btn-block viewButton">
								View
							</a>
							<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton">
								Edit
							</a>
							<a href="./deleteCourse.html?courseId=${course._id}" value={course._id} class="btn btn-danger text-white btn-block deleteButton">
								Disable
							</a>
						`
				} else {
						cardFooter =
						`
							<a href="./viewCourse.html?courseId=${course._id}" value={course._id} class="btn btn-success text-white btn-block viewButton">
								View
							</a>
							<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton">
								Edit
							</a>
							<a href="./activateCourse.html?courseId=${course._id}" value={course._id} class="btn btn-warning text-white btn-block deleteButton">
								Enable
							</a>
						`
				}
				return(
					`
						<div class="col-md-6 my-3">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">
										${course.name}
									</h5>
									<p class="card-text text-left">
										${course.description}
									</p>
									<p class="card-text text-right">
										₱${course.price}
									</p>
								</div>
								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>
					`
				)
			}

		}).join("") //<- to remove the , every after element caused by .map().
	}
	let container = document.querySelector("#coursesContainer")

	container.innerHTML = courseData;
})

