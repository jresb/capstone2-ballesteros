let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')

// get the token from the local storage
let token = localStorage.getItem('token')

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")

let isAdmin = localStorage.getItem('isAdmin') === 'true'
if (isAdmin === true) {
	console.log("Welcome, admin.")

	fetch(`https://immense-wildwood-11110.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {
		for (i=0; i < data.enrollees.length; i++) {
			console.log(data.enrollees[i].userId)
		}
		console.log("number of enrollees:", data.enrollees.length)

		courseName.innerHTML = data.name
		courseDesc.innerHTML = data.description
		coursePrice.innerHTML = data.price

		/*
		if (err) {
			enrollContainer.innerHTML = `There's currently no enrollees yet.`
		}
		*/

		if (data.enrollees.length < 1) {
			enrollContainer.innerHTML = `There's currently no enrollees yet.`
		} else {
			//shows the enrollees of the course in object format
			let userId
			let enrolleeName = ""
			data.enrollees.forEach( e => {
				console.log(e)
				userId = e.userId
				console.log(userId)

				fetch(`https://immense-wildwood-11110.herokuapp.com/api/users/${userId}`, {
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					console.log(enrolleeName)
					enrolleeName += `${data.firstName} ${data.lastName} <br>`
					enrollContainer.innerHTML = 
					`
						<p>Current Enrollees:</p>
						<p>${enrolleeName}</p>
					`
				})
			}) 
		}
	})
	.catch(err => {
		enrollContainer.innerHTML = `There's currently no enrollees yet.`
	})
} else {
	courseName.innerHTML = `<p>You are not an admin user</p>`
	document.querySelector(".secondary").innerHTML = ``
}












/*
			let userId
			let userNames = ""
			

				console.log(`Data Length: ${data.length}`)
				for (i=0; i < data.length; i++)
					userId = data._id
					console.log(userId)
					if (data.enrollees
			})

			for (i = 0; i < data.enrollees.length; i++) {
				userId = data.enrollees[i].userId
				
				if (data.enrollees.includes(userId)) {
					console.log()
				}*/

				/*

				*/
/*			}
		}
	})*/