// let userId = params.get('userId')
let token = localStorage.getItem('token')
let userId = localStorage.getItem('id')
console.log(userId)

let fName = document.querySelector('#firstName')
let lName = document.querySelector('#lastName')
let mNo = document.querySelector('#mobileNumber')
let uEmail = document.querySelector('#userEmail')
let pw1 = document.querySelector('#password1')
let pw2 = document.querySelector('#password2')


fetch(`https://immense-wildwood-11110.herokuapp.com/api/users/details`, {
	headers: {
		Authorization: `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
console.log(data)

fName.placeholder = data.firstName
lName.placeholder = data.lastName
mNo.placeholder = data.mobileNo
uEmail.placeholder = data.email

fName.value = data.firstName
lName.value = data.lastName
mNo.value = data.mobileNo
uEmail.value = data.email
})



let formSubmit = document.querySelector("#editUser")
formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()

	console.log("form submit initiated")

	let firstName = fName.value
	let lastName = lName.value
	let mobileNo = mNo.value
	let email = uEmail.value
	let password1 = pw1.value
	let password2 = pw2.value

	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)) {

			fetch(`https://immense-wildwood-11110.herokuapp.com/api/users`,{
				method: 'PUT', 
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},	
				body: JSON.stringify({
					id: userId,
					firstName: firstName,
					lastName: lastName,
					email: email,
					password : password1,
					mobileNo: mobileNo
				})
			})
			.then(res => res.json())
			.then(data => {
				if (data === true) {
					alert('Edit successful')
					window.location.replace('./profile.html')
				} else {
					alert("Something went wrong.")
				}
			})
	} else {
		alert("Something went wrong.")
	}
})


document.getElementById("cancel").addEventListener("click", function() {
	window.location.replace('./courses.html')
})