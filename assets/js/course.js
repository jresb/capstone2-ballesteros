//course.html?courseId=601cc9b6eb545e08d8a7e487
// URL Query parameters
/*
	? - start of query string
	courseId = (parameter name)
	601cc9b6eb545e08d8a7e487 = value
*/
// window.location.search return the query string in the URL
// console.log(window.location.search)

// instatitate or create a new URLSearchParams object
// This object, URLSearchParams, ise used an interface to gain access to methods that allows us to specific parts of the query string.

let params = new URLSearchParams(window.location.search)

// the has method for URLSearchParams checks if the courseId key exists in our URL Query string.
// console.log(params.has('courseId'))

// the get method for URLSearchParams returns the value of the key passed in as an argument
// console.log(params.get('courseId'))

// store the courseId from the URL Query string in a variable:
let courseId = params.get('courseId')

// get the token from the local storage
let token = localStorage.getItem('token')

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")

//not defined vs undefined
/*
	undefined = variable was declared without initial value
	not defined = the variable does not exist
*/

// get the details of a single course
// api/courses/:id
fetch(`https://immense-wildwood-11110.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	// console.log(data)
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`

	//this will change the enroll button to enrolled if the user is already enrolled in the said course
	fetch(`https://immense-wildwood-11110.herokuapp.com/api/users/details`, {
		headers: {
			Authorization: `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		// console.log(data)
		let isEnrolled = data.enrollments.map(x => x.courseId).includes(courseId)
		console.log(`Is ${data.firstName} currently enrolled in this course?: ${isEnrolled}`)
		return isEnrolled
	})
	.then(data => {
		// console.log(data)
		if (data === true) {
			enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary disabled">Enrolled</button>`
		}
	})

	document.querySelector("#enrollButton").addEventListener("click", () => {
		// no need to prevent default because click events doesn't refresh the page
		// add fetch request to enroll our user:

		if (!token) {
			// console.log("no token")
			window.location.replace('./register.html')
		} else {		
			fetch('https://immense-wildwood-11110.herokuapp.com/api/users/enroll', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId
				})
			})
			.then(res => res.json())
			.then(data => {
				//redirect the user to the courses page after enrolling.
				if (data === true) {
					// show an alert to than the user:
					alert('Thank you for enrolling to the course')
					window.location.replace('./courses.html')
				} else {
					// server error while enrolling to course
					alert('Somthing went wrong.')
				}
			})
		}
	})
})