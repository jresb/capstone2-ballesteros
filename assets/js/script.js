let navItems = document.querySelector("#navSession");
let registerLink = document.querySelector("#register");

//12 feb added code
// this is for the profile page link in the navbar
let profile = document.querySelector("#profile");
//end of new code

let userToken = localStorage.getItem("token");

if(!userToken) {
	navItems.innerHTML =
	`
		<li class="nav-item ">
			<a href="./login.html" class="nav-link"> Login </a>
		</li>
	`
	registerLink.innerHTML =
	`
		<li class="nav-item ">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>
	`
} else {
	//12 feb added code
	// this is for the profile page link in the navbar
	
	profile.innerHTML = 
	`
		<li class="nav-item ">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>	
	`
	
	//end of new code
	
	navItems.innerHTML =
	`
		<li class="nav-item ">
			<a href="./logout.html" class="nav-link"> Logout </a>
		</li>	
	`
	
}