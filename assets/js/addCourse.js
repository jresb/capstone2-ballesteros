let formSubmit = document.querySelector("#createCourse")
// console.log(formSubmit) <-for checking

// add an event listener
// this is the code when you press create course button
formSubmit.addEventListener("submit", (e) => {
	// Q: what does preventDefault() do?
	// A: it prevents the normal behavior of an event. In this case, the event is submitted and its default behavior is to refresh the page when submitting the form.
	e.preventDefault()

	// get the values of your input
	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value
	// use to determine if its working
	console.log("courseName:", courseName)
	console.log("description", description)
	console.log("price", price)

	// get the JWT from our localStorage
	let token = localStorage.getItem("token")
	console.log("token of admin:", token)

	// create a fetch request to add a new course
	let priceNum = price * 1 / price * 1
	if (courseName.length !== 0 && description.length !== 0 && price.length !== 0 && priceNum == 1) {

		fetch('http://localhost:8000/api/courses',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: courseName,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			// if the creation of the course is successful, redirect admin to the courses page
			if (data === true) {
				window.location.replace('./courses.html')
			} else {
				alert("Something went wrong.")
			}
		})
	} else {
		alert("Please complete the form.")
	}
})

document.getElementById("cancel").addEventListener("click", function() {
	// window.location.replace('./courses.html')
	document.querySelector("#courseName").value = ""
	document.querySelector("#courseDescription").value = ""
	document.querySelector("#coursePrice").value = ""
})
