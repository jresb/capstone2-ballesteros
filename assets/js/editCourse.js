let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')

fetch(`http://localhost:8000/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
// console.log(data)
document.querySelector('#courseName').placeholder = data.name
document.querySelector('#coursePrice').placeholder = data.price
document.querySelector('#courseDescription').placeholder = data.description
document.querySelector('#courseName').value = data.name
document.querySelector('#coursePrice').value = data.price
document.querySelector('#courseDescription').value = data.description
})


let formSubmit = document.querySelector("#editCourse")

//add an event listener

formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()

	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value

	// create a fetch request to add the updates of the course

	fetch(`http://localhost:8000/api/courses`,{
		method: 'PUT', //<-or siguro dahil dito? naglalagay siya ng bagong course instead na nag eedit. try niyo guys 'PUT'
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},	
		body: JSON.stringify({
			id: courseId,
			name: courseName,
			description: description,
			price: price
		})
	})
	.then(res => res.json())
	.then(data => {
		if (data === true) {
			alert('Edit successful')
			window.location.replace('./courses.html')
		} else {
			alert("Something went wrong.")
		}
	})
})

/*
// answer from sir Arvin
console.log(window.location.search)
let params = new URLSearchParams(window.location.search)
console.log(params.has('courseId'))

//get the courseId
let courseId = params.get('courseId')
console.log(courseId)

//select the form input fields
let name = document.querySelector("#courseName")
let description = document.querySelector("#courseDescription")
let price = document.querySelector("#coursePrice")

fetch(`http://localhost:8000/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	console.log(data)

	name.placeholder = data.name
	price.placeholder = data.price
	description.placeholder = data.description
	
	name.value = data.name
	price.value = data.price
	description.value = data.description
})

//add an event listener to the form
document.querySelector("#editCourse").addEventListener("submit", (e) => {
	e.preventDefault()

	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value

	let token = localStorage.getItem('token')

	fetch(`http://localhost:8000/api/courses`,{
		method: 'PUT', //<-or siguro dahil dito? naglalagay siya ng bagong course instead na nag eedit. try niyo guys 'PUT'
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},	
		body: JSON.stringify({
			id; courseId,
			name: courseName,
			description: description,
			price: price
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)

		if (data === true) {
			window.location.replace("./courses.html")
		} else {
			alert("Something went wrong.")
		}
	})
})
*/