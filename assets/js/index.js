let navItems = document.querySelector("#navSession");
let registerLink = document.querySelector("#register");
let userToken = localStorage.getItem("token");
let bookNow = document.querySelector("#book-now");
let profile = document.querySelector("#profile");

if(!userToken) {
	navItems.innerHTML =
	`
		<li class="nav-item ">
			<a href="./pages/login.html" class="nav-link"> Login </a>
		</li>
	`
	registerLink.innerHTML =
	`
		<li class="nav-item ">
			<a href="./pages/register.html" class="nav-link"> Register </a>
		</li>
	`
	bookNow.innerHTML = 
	`
	<a href="./pages/courses.html" class="btn btn-primary">Book Now</a>
	`


} else {
	navItems.innerHTML =
	`
		<li class="nav-item ">
			<a href="./pages/logout.html" class="nav-link"> Logout </a>
		</li>	
	`	
	bookNow.innerHTML = 
	`
	<a href="./pages/courses.html" class="btn btn-primary">Book Now</a>
	`
	profile.innerHTML =
	`
	<li class="nav-item ">
		<a href="./pages/profile.html" class="nav-link">Profile</a>
	</li>
	`
}