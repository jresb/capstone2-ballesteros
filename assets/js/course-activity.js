let token = localStorage.getItem("token")
let adminUser = localStorage.getItem("isAdmin") === "true"
let addButton = document.querySelector("#adminButton")

// this if statement will allow us to show a button for adding a course/button to redirect us to the addCourse page it he user is admin
// otherwise, the button won't show
if (adminUser === false || adminUser === null) {
	addButton.innerHTML = null
} else {
	// addButton.innerHTML = `<h3>User is an Admin</h3>`
	addButton.innerHTML = `
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
		</div>
	`
}

/*
	Activity:

	use the fetch Method to get all the courses and show the data into the console using console.log()

	If you are done, create a new folder (s19) in gitlab
	inside s19 folder, create a new repo: d1

	In your local machine, connect your d1 to your online repo:
	git remote add origin <url>

	Then add, commit and push it into your new repo.

	Link to boodle as:
	WD057-11 | Express.js - Booking System API Integration


*/

fetch('https://immense-wildwood-11110.herokuapp.com/api/courses/')
.then(res => res.json())
.then(data => {
	console.log(data)
})